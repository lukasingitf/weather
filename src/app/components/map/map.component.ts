import { Component, OnInit } from '@angular/core';
import { MouseEvent } from '@agm/core';
import { WeatherDataService } from 'src/app/services/weather-data.service';

@Component({
  selector: 'app-map',
  template: `
    <agm-map style="height:80vh;" [latitude]="lat" [longitude]="lng">
      <agm-marker
        [latitude]="marker.lat"
        [longitude]="marker.lng"
        [markerDraggable]="marker.draggable"
        (dragEnd)="markerDragEnd($event)"
      ></agm-marker>
    </agm-map>
  `,
})
export class MapComponent implements OnInit {
  public lat: number = 40.24;
  public lng: number = -111.66;
  public marker: any = {
    lat: this.lat,
    lng: this.lng,
    draggable: true,
  };

  constructor(private service: WeatherDataService) {}

  ngOnInit(): void {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.lat = position.coords.latitude;
        this.lng = position.coords.longitude;
        this.marker = {
          lat: this.lat,
          lng: this.lng,
          draggable: true,
        };
        this.service.loadData(this.lat, this.lng);
      });
    }
    this.service.loadData(this.lat, this.lng);
  }

  markerDragEnd($event: MouseEvent) {
    this.service.loadData($event.coords.lat, $event.coords.lng);
  }
}
