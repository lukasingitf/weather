import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { WeatherDataService } from '../../services/weather-data.service';

@Component({
  selector: 'app-weather-table',
  styles: [
    `
      th {
        text-align: right !important;
        width: 30%;
      }
    `,
  ],
  template: `
    <table *ngIf="data" class="table table-md">
      <tbody>
        <tr class="table-warning">
          <th scope="row">Location</th>
          <td>{{ data.name | titlecase }}</td>
        </tr>
        <tr class="table-info">
          <th scope="row">Condition</th>
          <td>{{ data.weather[0].description | titlecase }}</td>
        </tr>
        <tr class="table-warning">
          <th scope="row">Temperature</th>
          <td>{{ data.main.temp }} F</td>
        </tr>
        <tr class="table-info">
          <th scope="row">Humidity</th>
          <td>{{ data.main.humidity }}%</td>
        </tr>
        <tr class="table-warning">
          <th scope="row">Sunrise</th>
          <td>{{ data.sys.sunrise * 1000 | date: 'HH:mm' }}</td>
        </tr>
        <tr class="table-info">
          <th scope="row">Sunset</th>
          <td>{{ data.sys.sunset * 1000 | date: 'HH:mm' }}</td>
        </tr>
        <tr class="table-warning">
          <th scope="row">Wind Speed</th>
          <td>{{ data.wind.speed }} mph</td>
        </tr>
        <tr class="table-info">
          <th scope="row">Wind Direction</th>
          <td colspan="2">{{ data.wind.deg }} degrees</td>
        </tr>
      </tbody>
    </table>
  `,
})
export class WeatherTableComponent implements OnInit {
  public subscription: Subscription;
  public data: any;

  constructor(private service: WeatherDataService) {}

  ngOnInit(): void {
    this.subscription = this.service.currentLocationData.subscribe(
      (val) => (this.data = val)
    );
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
