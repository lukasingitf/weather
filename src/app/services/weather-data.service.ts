import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class WeatherDataService {
  private locationData = new Subject();
  currentLocationData = this.locationData.asObservable();

  private url: string = 'https://api.openweathermap.org/data/2.5/weather';
  private api: string = '8eb102c755f05f198cf66a06ccf2dd4c';
  private units: string = 'imperial';
  constructor(private http: HttpClient) {}

  public loadData(lat: number, lon: number) {
    let params = new HttpParams()
      .set('lat', lat.toString())
      .set('lon', lon.toString())
      .set('appid', this.api)
      .set('units', this.units);

    this.http.get(this.url, { params }).subscribe((res) => {
      this.locationData.next(res);
    });
  }
}
