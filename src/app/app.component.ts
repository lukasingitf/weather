import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
    <div class="container">
      <div class="row">
        <div class="col-sm text-center p-3">
          <h4>
            Click a point on the map to see current weather information for that
            location
          </h4>
        </div>
      </div>
    </div>
    <div class="container">
      <div class="row">
        <div class="col-sm">
          <app-map></app-map>
        </div>
        <div class="col-sm">
          <app-weather-table></app-weather-table>
        </div>
      </div>
    </div>
  `,
})
export class AppComponent {
  title = 'weather-map';
}
